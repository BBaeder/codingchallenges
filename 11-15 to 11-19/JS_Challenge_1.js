// 1) Write two functions:

// - One to retrieve all unique substrings that start and end with a vowel.
// - One to retrieve all unique substrings that start and end with a consonant.
// The resulting array should be sorted in lexicographic ascending order (same order as a dictionary).

// - Remember the output array should have unique values.
// - The word itself counts as a potential substring.
// - Exclude the empty string when outputting the array.

function getVowelSubstrings(str) {
    let arr = [];
    for (let x = 0; x < str.length; x++) {
        const c1 = str.charAt(x);
        for (y = x; y < str.length; y++) {
            const c2 = str.charAt(y);
            if (c1.toUpperCase() === 'A' || c1.toUpperCase() === 'E' || c1.toUpperCase() === 'I' || c1.toUpperCase() === 'O' || c1.toUpperCase() === 'U') {
                if (c2.toUpperCase() === 'A' || c2.toUpperCase() === 'E' || c2.toUpperCase() === 'I' || c2.toUpperCase() === 'O' || c2.toUpperCase() === 'U') {
                    arr.push(str.substring(x, y + 1));
                }
            }
        }
    }
    return arr;
}

function getConsonantSubstrings(str) {
    let arr = [];
    for (let x = 0; x < str.length; x++) {
        const c1 = str.charAt(x);
        for (y = x; y < str.length; y++) {
            const c2 = str.charAt(y);
            if (c1.toUpperCase() !== 'A' && c1.toUpperCase() !== 'E' && c1.toUpperCase() !== 'I' && c1.toUpperCase() !== 'O' && c1.toUpperCase() !== 'U') {
                if (c2.toUpperCase() !== 'A' && c2.toUpperCase() !== 'E' && c2.toUpperCase() !== 'I' && c2.toUpperCase() !== 'O' && c2.toUpperCase() !== 'U') {
                    arr.push(str.substring(x, y + 1));
                }
            }
        }
    }
    return arr;
}

// Examples:
console.log(getVowelSubstrings("apple"))
// --> ["a", "apple", "e"]

console.log(getVowelSubstrings("hmm"))
// --> []

console.log(getConsonantSubstrings("aviation"))
// --> ["n", "t", "tion", "v", "viat", "viation"]

console.log(getConsonantSubstrings("motor"))
// --> ["m", "mot", "motor", "r", "t", "tor"]

console.log(getConsonantSubstrings("aaaaeeeooooiiiiiuuuuuuuu"))
// --> []
