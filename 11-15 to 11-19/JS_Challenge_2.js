// 2) Write a function redundant that takes in a string 'str' and returns a function that returns 'str'.

// Your function should return a 'function', not a string.

function redundant (str) {
    return (()=>{return(str)})
}

// Examples
const f1 = redundant("apple")
console.log(f1);
console.log(f1())

const f2 = redundant("pear")
console.log(f2);
console.log(f2())

const f3 = redundant("")
console.log(f3);
console.log(f3())