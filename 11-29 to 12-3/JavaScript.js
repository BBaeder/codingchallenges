// Simulate the game "Rock, Paper, Scissors"
// Rock beats scissors, paper beats rock, scissors beat paper.
// Create a function that simulates the game "rock, paper, scissors". 
// The function takes the input of both players (rock, paper, or scissors), 
// the first parameter from the first player, second from the second player. 
// The function returns the result as such:

// "Player 1 wins"
// "Player 2 wins"
// "TIE" (if both inputs are the same)

function rps(p1, p2) {
    if (p1 === p2) {
        console.log("TIE");
    } else {
        if(p1 === "rock" && p2 === "paper") {
            console.log("Player 2 wins")
        } else if (p1 === "rock" && p2 === "scissors"){
            console.log("Player 1 wins")
        } else if (p1 === "paper" && p2 === "scissors"){
            console.log("Player 2 wins")
        } else if (p1 === "paper" && p2 === "rock"){
            console.log("Player 1 wins")
        } else if (p1 === "scissors" && p2 === "rock"){
            console.log("Player 2 wins")
        } else if (p1 === "scissors" && p2 === "paper"){
            console.log("Player 1 wins")
        }
    }
}

// Examples
rps("rock", "paper") //➞ "Player 2 wins"
rps("paper", "rock") //➞ "Player 1 wins"
rps("paper", "scissors") //➞ "Player 2 wins"
rps("scissors", "scissors") //➞ "TIE"
rps("scissors", "paper") //➞ "Player 1 wins"