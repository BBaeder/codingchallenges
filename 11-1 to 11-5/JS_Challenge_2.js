// 2) Write a function that mimics (without the use of >>) the right shift operator and returns the result from the two given integers.

function shiftToRight(n1, n2) {
    return Math.floor((n1/(2 ** n2)))
}

console.log(shiftToRight(80, 3))

console.log(shiftToRight(-24, 2))

console.log(shiftToRight(-5, 1))

console.log(shiftToRight(4666, 6))

console.log(shiftToRight(3777, 6))

console.log(shiftToRight(-512, 10))

// Examples
// shiftToRight(80, 3) --> 10

// shiftToRight(-24, 2) --> -6

// shiftToRight(-5, 1) --> -3

// shiftToRight(4666, 6) --> 72

// shiftToRight(3777, 6) --> 59

// shiftToRight(-512, 10) --> -1