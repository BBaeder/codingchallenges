// 1) Write a function that moves all the zeroes to the end of an array. Do this without returning a copy of the input array.

function zeroesToEnd(arr) {
    let zeroCount = 0;
    let i = 0;
    while ( i < arr.length) {
        if (arr[i] === 0) {
            arr.splice(i,1);
            zeroCount++;
        } else {
            i++;
        }
    }
    for(let x = 0; x < zeroCount; x++) {
        arr.push(0)
    }
    return arr;
}

console.log(zeroesToEnd([1, 2, 0, 0, 4, 0, 5]))

console.log(zeroesToEnd([0, 0, 2, 0, 5]))

console.log(zeroesToEnd([4, 4, 5]))

console.log(zeroesToEnd([0, 0]))

// zeroesToEnd([1, 2, 0, 0, 4, 0, 5]) --> [1, 2, 4, 5, 0, 0, 0]

// zeroesToEnd([0, 0, 2, 0, 5]) --> [2, 5, 0, 0, 0]

// zeroesToEnd([4, 4, 5]) --> [4, 4, 5]

// zeroesToEnd([0, 0]) --> [0, 0]
// Notes:

// -You must mutate the original array.
// -Keep the relative order of the non-zero elements the same.