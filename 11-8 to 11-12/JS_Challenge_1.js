// 1) This robot roams around a 2D grid. It starts at (0, 0) facing North. After each time it moves, the robot rotates 90 degrees clockwise. 
// Given the amount the robot has moved each time, you have to calculate the robot's final position.

// To illustrate, if the robot is given the movements 20, 30, 10, 40 then it will move:

// 20 steps North, now at (0, 20)
// 30 steps East, now at (30, 20)
// 10 steps South. now at (30, 10)
// 40 steps West, now at (-10, 10)
// ...and will end up at coordinates (-10, 10).

function trackRobot(n, e, s, w){
    let pos = [0,0];
    if (n === undefined) {
        n = 0;
    }
    if (e === undefined) {
        e = 0;
    }
    if (s === undefined) {
        s = 0;
    }
    if (w === undefined) {
        w = 0;
    }
    pos[0] = e - w;
    pos[1] = n - s;
    return pos;
}

// trackRobot([20, 30, 10, 40]) ➞ [-10, 10]
console.log(trackRobot(20,30,10,40));

// trackRobot() ➞ [0, 0]
// No movement means the robot stays at (0, 0).
console.log(trackRobot());

// trackRobot([-10, 20, 10]) ➞ [20, -20]
// The amount to move can be negative.
console.log(trackRobot(-10,20,10))